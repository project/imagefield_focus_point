<?php

/**
 * @file
 * Defines imagefield_focus_point hooks implementations, callbacks & themes.
 */

/**
 * Implements hook_element_info_alter().
 */
function imagefield_focus_point_element_info_alter(&$type) {
  if (array_key_exists('media', $type)) {
    array_push($type['media']['#process'], 'imagefield_focus_point_media_element_process');
  }
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function imagefield_focus_point_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'imagefield_focus_point_style_focus_point':
      $options = array();
      foreach (image_styles() as $name => $style) {
        $options[$name] = $style['label'];
      }
      $element['imagefield_focus_point_image_style_crop'] = array(
        '#type'          => 'select',
        '#title'         => t('Crop image style'),
        '#options'       => $options,
        '#default_value' => $settings['imagefield_focus_point_image_style_crop'],
      );
      $element['imagefield_focus_point_image_style_src'] = array(
        '#type'          => 'select',
        '#title'         => t('Source image style'),
        '#options'       => $options,
        '#default_value' => $settings['imagefield_focus_point_image_style_src'],
      );
      break;
  }
  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function imagefield_focus_point_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $summary = array();

  switch ($display['type']) {
    case 'imagefield_focus_point_style_focus_point':
      $styles = image_styles();
      array_push($summary, t('Crop image style: @style', array('@style' => $styles[$settings['imagefield_focus_point_image_style_crop']]['label'])));
      array_push($summary, t('Source image style: @style', array('@style' => $styles[$settings['imagefield_focus_point_image_style_src']]['label'])));
      break;
  }
  return implode('<br />', $summary);
}

/**
 * Implements hook_field_widget_form_alter().
 */
function imagefield_focus_point_field_widget_form_alter(&$element, &$form_state, $context) {
  $widget_type = $context['instance']['widget']['type'];
  if (imagefield_focus_point_widget_support($widget_type)) {
    foreach (element_children($element) as $delta) {
      $element[$delta]['#process'][] = 'imagefield_focus_point_widget_' . $widget_type . '_process';
    }
  }
}

/**
 * Implements hook_field_attach_insert().
 */
function imagefield_focus_point_field_attach_insert($entity_type, $entity) {
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);

  foreach (field_info_instances($entity_type, $bundle) as $instance) {
    if (imagefield_focus_point_widget_support($instance['widget']['type'])) {
      $field_name = $instance['field_name'];
      $field = field_info_field($field_name);
      $available_languages = field_available_languages($entity_type, $field);
      $languages = _field_language_suggestion($available_languages, NULL, $field_name);

      foreach ($languages as $langcode) {
        $items = isset($entity->{$field_name}[$langcode]) ? $entity->{$field_name}[$langcode] : array();
        foreach ($items as $item) {
          $file = (object) $item;
          $file->uri = file_load($file->fid)->uri;
          _imagefield_focus_point_file_save($file);
        }
      }
    }
  }
}

/**
 * Implements hook_query_TAG_alter().
 */
function imagefield_focus_point_query_file_load_multiple_alter(QueryAlterableInterface $query) {
  $query->fields('IFPF', array('imagefield_focus_point_x', 'imagefield_focus_point_y'));
  $query->leftJoin('imagefield_focus_point_file', 'IFPF', 'IFPF.fid = base.fid');
}

/**
 * Implements hook_field_attach_update().
 */
function imagefield_focus_point_field_attach_update($entity_type, $entity) {
  imagefield_focus_point_field_attach_insert($entity_type, $entity);
}

/**
 * Implements hook_theme_registry_alter().
 */
function imagefield_focus_point_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['image'])) {
    $theme_registry['image']['function'] = 'theme_imagefield_focus_point_theme_image';
    $theme_registry['image_style']['function'] = 'theme_imagefield_focus_point_theme_image_style';
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function imagefield_focus_point_field_formatter_info() {
  return array(
    'imagefield_focus_point_style_focus_point' => array(
      'label'       => t('Styled, focal pointed image'),
      'field types' => array('image'),
      'settings'    => array(
        'imagefield_focus_point_image_style_crop' => '',
        'imagefield_focus_point_image_style_src' => '',
        'imagefield_focus_point_x'                => '',
        'imagefield_focus_point_y'                => '',
      ),
      'description' => t('Intelligent cropping for flexible image containers.'),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function imagefield_focus_point_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

  $element = array();
  $variables = array('settings' => $display['settings']);
  foreach ($items as $delta => $item) {
    $variables['item'] = $item;

    switch ($display['type']) {

      case 'imagefield_focus_point_style_focus_point':
        $file = file_load($variables['item']['fid']);
        $info = image_get_info($file->uri);
        $variables['style_name_crop'] = $variables['settings']['imagefield_focus_point_image_style_crop'];
        $variables['style_name_src']  = $variables['settings']['imagefield_focus_point_image_style_src'];
        $variables['path']            = $file->uri;
        $variables['width']           = $info['width'];
        $variables['height']          = $info['height'];
        $element[$delta] = array('#markup' => theme('image_style', $variables));
        break;
    }
  }
  return $element;
}

/**
 * Implements hook_page_build().
 */
function imagefield_focus_point_page_build(&$page) {
  module_load_include('inc', 'imagefield_focus_point', 'imagefield_focus_point');
  drupal_add_js(imagefield_focus_point_library('js'), array('scope' => 'header'));
  drupal_add_css(imagefield_focus_point_library('css'));
}

/**
 * Implements hook_theme().
 */
function imagefield_focus_point_theme() {
  return array(
    // Theme functions in image.module.
    'image_style_focus_point' => array(
      'variables' => array(
        'style_name_crop'          => NULL,
        'style_name_src'           => NULL,
        'path'                     => NULL,
        'width'                    => NULL,
        'img_width'                => NULL,
        'height'                   => NULL,
        'img_height'               => NULL,
        'alt'                      => '',
        'title'                    => NULL,
        'attributes'               => array(),
        'imagefield_focus_point_x' => NULL,
        'imagefield_focus_point_y' => NULL,
      ),
    ),
  );
}

/**
 * Validation of imagefield_focus_point_x and imagefield_focus_point_y.
 */
function imagefield_focus_point_validate_range($element, &$form_state) {
  $value = $element['#value'];
  if ($value > 1 or $value < -1) {
    form_error($element, t('%name must be with range of -1 to +1.', array('%name' => $element['#title'])));
  }
}

/**
 * Callback for hook_element_info #process.
 */
function imagefield_focus_point_media_element_process(&$element, &$form_state, $form) {
  return imagefield_focus_point_widget_process($element, $form_state, $form);
}

/**
 * Support for media_generic widget (https://www.drupal.org/project/media).
 */
function imagefield_focus_point_widget_media_generic_process($element, &$form_state, $form) {
  // Processing is added via hook_element_info_alter.
}

/**
 * Support for image widget (core module).
 */
function imagefield_focus_point_widget_image_image_process($element, &$form_state, $form) {
  return imagefield_focus_point_widget_process($element, $form_state, $form);
}

/**
 * Widget elemment form (X & Y focus).
 */
function imagefield_focus_point_widget_process($element, &$form_state, $form) {

  $item = $element['#value'];

  // Add focus point fields.
  $element['imagefield_focus_point_x'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Focus point X'),
    '#default_value'    => isset($item['imagefield_focus_point_x']) ? $item['imagefield_focus_point_x'] : 0,
    '#access'           => (bool) $item['fid'],
    '#required'         => TRUE,
    '#element_validate' => array('element_validate_number'),
    '#attributes'       => array('class' => array('imagefield-focus-point-x', 'imagefield_focus_point_validate_range')),
  );
  $element['imagefield_focus_point_y'] = array(
    '#type'             => 'textfield',
    '#title'            => t('Focus point Y'),
    '#default_value'    => isset($item['imagefield_focus_point_y']) ? $item['imagefield_focus_point_y'] : 0,
    '#access'           => (bool) $item['fid'],
    '#required'         => TRUE,
    '#element_validate' => array('element_validate_number', 'imagefield_focus_point_validate_range'),
    '#attributes'       => array('class' => array('imagefield-focus-point-y')),
  );
  if (array_key_exists('fid', $item) and $file = file_load($item['fid'])) {
    $element['imagefield_focus_point_picker'] = array(
      'text' => array(
        '#markup' => t('Click on the image below to set the focus point.'),
      ),
      'image' => array(
        '#theme'      => 'image_style',
        '#path'       => $file->uri,
        '#style_name' => 'large',
        '#attributes' => array(
          'class' => array('imagefield-focus-point-picker'),
        ),
      ),
    );
  }
  return $element;
}

/**
 * Return whether Imagefield focus point has support for the given widget.
 */
function imagefield_focus_point_widget_support($type) {
  return function_exists('imagefield_focus_point_widget_' . $type . '_process');
}

/**
 * Save focus data.
 */
function _imagefield_focus_point_file_save($file) {
  if (empty($file->imagefield_focus_point_x) && empty($file->imagefield_focus_point_x)) {
    $affected = _imagefield_focus_file_delete($file);
  }
  else {
    db_merge('imagefield_focus_point_file')
      ->key(array('fid' => $file->fid))
      ->fields(array(
        'imagefield_focus_point_x'           => @$file->imagefield_focus_point_x,
        'imagefield_focus_point_y'           => @$file->imagefield_focus_point_y,
      ))
      ->execute();
    $affected = TRUE;
  }

  if ($affected) {
    image_path_flush($file->uri);
  }
}

/**
 * Wrapper for theme_image.
 */
function theme_imagefield_focus_point_theme_image($variables) {

  if (array_key_exists('settings', $variables) and array_key_exists('imagefield_focus_point_x', $variables['settings']) and array_key_exists('imagefield_focus_point_y', $variables['settings'])) {
    $attributes_wrap = array(
      'style' => 'height:' . $variables['img_height'] . 'px;' . 'width:' . $variables['img_width'] . 'px;',
    );
    $attributes_div = array(
      'class'        => array('focuspoint'),
      'data-focus-x' => $variables['item']['imagefield_focus_point_x'],
      'data-focus-y' => $variables['item']['imagefield_focus_point_y'],
      'data-image-w' => $variables['width'],
      'data-image-h' => $variables['height'],
    );

    // Determine the source (uncropped source).
    $style_name = array_key_exists('style_name_src', $variables) ? $variables['style_name_src'] : 'large';
    if ($path = image_style_url($style_name, $variables['path'])){
      $variables['path'] = $path;
    }
    return '
      <div ' . drupal_attributes($attributes_wrap) . '>
        <div ' . drupal_attributes($attributes_div) . '>' . theme_image($variables) . '</div>
      </div>
    ';
  }
  else {
    return theme_image($variables);
  }
}

/**
 * Wrapper for theme_image_style.
 */
function theme_imagefield_focus_point_theme_image_style($variables) {
  if (array_key_exists('settings', $variables) and array_key_exists('imagefield_focus_point_x', $variables['settings']) and array_key_exists('imagefield_focus_point_y', $variables['settings'])) {
    $dimensions = array(
      'width'  => $variables['width'],
      'height' => $variables['height'],
    );
    image_style_transform_dimensions($variables['style_name_crop'], $dimensions);
    $variables['img_width']  = $dimensions['width'];
    $variables['img_height'] = $dimensions['height'];
    return theme_imagefield_focus_point_theme_image($variables);
  }
  else {
    return theme_image_style($variables);
  }
}
