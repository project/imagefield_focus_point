/**
 * @file
 * Defines imagefield_focus_point Javascript.
 */

(function($)  {
  function imagefield_focus_point(context)  {
    $(document).ready(function(){
      $('.focuspoint').focusPoint();
    });
  }

  Drupal.behaviors.imagefield_focus_point = {
    attach: function(context)  {
      imagefield_focus_point(context);

      // Determine position of click and enter co-ordinates in respective fields.
      jQuery("img.imagefield-focus-point-picker").click(function(e){

        var offset = $(this).offset();
        var relativeX = (e.pageX - offset.left);
        var relativeY = (e.pageY - offset.top);

        var originX = $(this).width() / 2;
        var focusX = 0;
        if (originX > relativeX){
          var origDist = originX - relativeX;
          focusX = 0 - (origDist / originX);
        } else if (originX < relativeX){
          var origDist = relativeX - originX;
          focusX = origDist / originX;
        }
        $(this).parent().find('div input.imagefield-focus-point-x').val(focusX);

        var originY = $(this).height() / 2;
        var focusY = 0;
         if (originY > relativeY){
          var origDist = originY - relativeY;
          focusY = origDist / originY;
        } else if (originY < relativeY){
          var origDist = relativeY - originY;
          focusY = 0 - (origDist / originY);
        }
        $(this).parent().find('div input.imagefield-focus-point-y').val(focusY);

      });
    }
  }
})(jQuery);
