<?php

/**
 * @file
 * Defines imagefield_focus_point common functions.
 */

/**
 * Library paths.
 */
function imagefield_focus_point_library($type) {
  $path = libraries_get_path('jquery.focuspoint');
  switch ($type) {
    case 'js':
      $path = implode('/', array($path, 'js', 'jquery.focuspoint.min.js'));
      break;

    case 'css':
      $path = implode('/', array($path, 'css', 'focuspoint.css'));
      break;
  }
  return $path;
}
