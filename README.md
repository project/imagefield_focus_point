# Imagefield focus point

## Overview
"Intelligent cropping for flexible image containers."  This module exposes an
image formatter which utilises the library 
[jquery-focuspoint](https://github.com/jonom/jquery-focuspoint) by jonom.

## Installation
* Download and install in the normal manner.
* Select 'Styled, focal pointed image' for an image field on display tab (eg
display settings of a content type or vocabulary). Set a image style for the
focus cropping.
* On a node/term with the field, click the focus point (JS will set the X / Y
co-ordinate fields).

## Supported widgets
* Image (Drupal core)
* Media file selector ([Media](https://www.drupal.org/project/media) module)

## Dependencies
In addition to the dependancy modules listed in the info file, the
jquery-focuspoint library must be located in the libraries directory: 
"sites/all/libraries/jquery.focuspoint", so that the two following paths are
valid:
* sites/all/libraries/jquery.focuspoint/css/focuspoint.css
* sites/all/libraries/jquery.focuspoint/js/jquery.focuspoint.min.js
